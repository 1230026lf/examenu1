package upv.com.examenu1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    int d2 = 1, m2 = 1, a2 = 2015;
    int dr, mr, ar, totald, totalmil;
    EditText d1, m1, a1;
    Button calcular;
    TextView ver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        d1 = (EditText) findViewById(R.id.dia1);
        m1 = (EditText) findViewById(R.id.mes1);
        a1 = (EditText) findViewById(R.id.anio1);
        calcular = (Button) findViewById(R.id.calcular);
        ver = (TextView) findViewById(R.id.ver);

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ar = Integer.parseInt(a1.getText().toString()) - a2;
                mr = Integer.parseInt(m1.getText().toString()) - m2;
                dr = Integer.parseInt(d1.getText().toString()) - d2;
                totald = (ar * 365) + (mr * 30) + dr;
                totalmil = totald * 3600000;
                ver.setText(totalmil + "");
            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
